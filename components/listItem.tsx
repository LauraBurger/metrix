import React from 'react';
import { ListItem as MuiListItem, ListItemIcon, ListItemText } from '@mui/material';
import Link from 'next/link';
import { ListItemProps } from '../types';

function ListItem({ href, icon, primary }: ListItemProps) {
  return (
    <MuiListItem button>
      <ListItemIcon>
        <Link href={href} passHref>
          {icon}
        </Link>
      </ListItemIcon>
      <ListItemText primary={primary} />
    </MuiListItem>
  );
}

export default ListItem;
