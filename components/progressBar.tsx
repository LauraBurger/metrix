import React from 'react';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import LinearProgress from '@mui/material/LinearProgress';

interface ProgressBarProps {
  title: string,
  value: number,
}

function getBoundValue(value: number) {
  let boundValue = value;

  if (value > 100) {
    boundValue = 100;
  } else if (value < 0) {
    boundValue = Math.abs(value);
  }

  return boundValue;
}

function ProgressBar({ title, value } : ProgressBarProps) {
  const coloredValue = value > 0 ? 'primary' : 'secondary';

  return (
    <Grid item xs={12} sx={{ typography: 'body2' }}>
      <Stack direction="row" spacing={1} sx={{ width: '100%' }}>
        <span>{title}</span>
        <Box sx={{ display: 'flex', alignItems: 'center', width: '100%' }}>
          <Box sx={{ width: '100%', mr: 1 }}>
            <LinearProgress variant="determinate" value={getBoundValue(value)} color={coloredValue} />
          </Box>
          <Box sx={{ minWidth: 35 }}>
            <Typography variant="body2" color="text.secondary">
              {`${Math.round(value)}%`}
            </Typography>
          </Box>
        </Box>
      </Stack>
    </Grid>
  );
}

export default ProgressBar;
