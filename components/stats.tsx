import Typography from '@mui/material/Typography';
import React from 'react';
import data from '../assets/products.json';
import Title from './title';

function Stats() {
  return (
    <>
      <Title>Assets</Title>
      <Typography component="p" variant="h4">
        {`${data.length} units`}
      </Typography>
      <Typography color="text.secondary" sx={{ flex: 1 }}>
        On 22 Feb, 2022
      </Typography>
    </>
  );
}

export default Stats;
