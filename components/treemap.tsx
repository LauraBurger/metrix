import { IgrTreemapModule, IgrTreemap, IgrTreemapNodePointerEventArgs } from 'igniteui-react-charts';
import React from 'react';
import { TreeMapData } from '../types/treemap';

IgrTreemapModule.register();

type TreeMapProps = {
  data: TreeMapData,
  title: string,
  nodePointerPressed: (s: IgrTreemap, e: IgrTreemapNodePointerEventArgs) => void,
};

export default function Treemap({ data, title, nodePointerPressed }: TreeMapProps) {
  return (
    <IgrTreemap
      height="500px"
      valueMemberPath="prop"
      rootTitle={title}
      parentIdMemberPath="parent"
      labelMemberPath="name"
      idMemberPath="id"
      dataSource={data}
      fillBrushes="rgb(246, 53, 56) rgb(49, 204, 90)"
      fillScaleMode="Value"
      isFillScaleLogarithmic="true"
      fillScaleMinimumValue="0"
      fillScaleMaximumValue="1500000000"
      headerDisplayMode="Overlay"
      parentNodeLeftPadding="0"
      parentNodeTopPadding="0"
      parentNodeRightPadding="0"
      parentNodeBottomPadding="0"
      outline="black"
      strokeThickness="1"
      nodePointerPressed={nodePointerPressed}
    />
  );
}
