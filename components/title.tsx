import Typography from '@mui/material/Typography';
import React from 'react';

interface TitleProps {
  children?: React.ReactNode;
}

function Title({ children }: TitleProps) {
  return (
    <Typography component="h2" variant="h6" color="primary" gutterBottom>
      {children}
    </Typography>
  );
}

export default Title;
