import React from 'react';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Link from 'next/link';
import Typography from '@mui/material/Typography';

interface CardClassNames {
  card?: string,
  title?: string
}

interface CardProps {
  color: string,
  header: string,
  title: string,
  subtitle: string | null,
  link: string,
  classNames: CardClassNames
}

function BasicCard({
  color, classNames, header, title, subtitle, link,
}: CardProps) {
  return (
    <Card sx={{ minWidth: 275, backgroundColor: `${color}` }} className={classNames.card}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          {header}
        </Typography>
        <Typography variant="h5" component="div" className={classNames.title}>
          {title}
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          {subtitle}
        </Typography>
      </CardContent>
      <CardActions>
        <Link href={link} passHref>
          <Button size="small">
            Details
          </Button>
        </Link>
      </CardActions>
    </Card>
  );
}

export default BasicCard;
