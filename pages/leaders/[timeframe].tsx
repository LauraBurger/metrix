import React from 'react';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import Stack from '@mui/material/Stack';
import Chip from '@mui/material/Chip';
import Link from 'next/link';
import { GetStaticPropsContext } from 'next';
import { ParsedUrlQuery } from 'querystring';
import Stats from '../../components/stats';
import Title from '../../components/title';
import { Timeframe, Timeframes } from '../../types/constants';
import { TreeMapData, TreeMapAsset, TreeMapProduct } from '../../types/treemap';
import rawData from '../../assets/products.json';
import Products from '../../utils/products';
import styles from '../../styles/timeframe.module.css';

interface IParams extends ParsedUrlQuery {
  timeframe: Timeframe
}

export async function getStaticProps(context : GetStaticPropsContext) {
  const { timeframe } = context.params as IParams;
  const treeMapAssets: TreeMapAsset[] = Products.getTreeMapAssets(rawData, timeframe);
  const treeMapProducts: TreeMapProduct[] = Products.getTreeMapProducts(rawData, timeframe);
  const treeMapData: TreeMapData = [...treeMapAssets, ...treeMapProducts];

  return {
    props: {
      data: treeMapData,
    },
  };
}

export async function getStaticPaths() {
  const paths = Timeframes.map((timeframe) => ({ params: { timeframe } }));

  return {
    paths,
    fallback: false,
  };
}

interface TimeframeProps {
  data: TreeMapData
}

const Treemap = dynamic(
  () => import('../../components/treemap'),
  { ssr: false },
);

function getLeaderLinks(currentTimeframe: Timeframe) {
  return Timeframes.map((timeframe) => {
    const isDisabled = currentTimeframe === timeframe;
    return (
      <Link href={`/leaders/${timeframe}`} passHref key={timeframe}>
        <Chip
          label={timeframe}
          variant="filled"
          color="primary"
          clickable
          disabled={isDisabled}
        />
      </Link>
    );
  });
}

function TimeFrame({ data } : TimeframeProps) {
  const router = useRouter();
  const timeframe = router.query.timeframe as Timeframe;

  const onProductClick = React.useCallback((s, e) => {
    const product = e.item as TreeMapProduct;
    if (product.parent) router.push(`/product/${product.id}`);
  }, [router]);

  const leaderLinks = getLeaderLinks(timeframe);
  const treeMapData: TreeMapData = data;

  return (
    <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={8} lg={9}>
          <Paper
            sx={{
              p: 2,
              display: 'flex',
              flexDirection: 'column',
              height: 240,
            }}
          >
            <Title>Timeframe</Title>
            <Stack
              direction="row"
              spacing={1}
              sx={{ mt: 1, mb: 1 }}
            >
              {leaderLinks}
            </Stack>
          </Paper>
        </Grid>
        <Grid item xs={12} md={4} lg={3}>
          <Paper className={styles['stats-paper']}>
            <Stats />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
            <Treemap data={treeMapData} title="ETF" nodePointerPressed={onProductClick} />
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
}

export default TimeFrame;
