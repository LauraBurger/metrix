import React from 'react';
import type { NextPage } from 'next';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import LazyLoad from 'react-lazyload';
import data from '../assets/products.json';
import Card from '../components/card';
import Stats from '../components/stats';
import Title from '../components/title';
import { Product } from '../types';
import Colors from '../utils/colors';
import Products from '../utils/products';

interface DashboardState {
  pattern: string,
}

interface DashboardProps {
  products: Product[],
}

export async function getStaticProps() {
  return {
    props: {
      products: data,
    },
  };
}

function getProductCards(products: Product[]) {
  return products.map((product) => (
    <Grid item xs={12} sm={12} md={4} lg={4} key={product.ISIN}>
      <LazyLoad scrollContainer=".cards-container">
        <Card
          classNames={{ card: 'card', title: 'truncate' }}
          color={Colors.getColor(product.asset)}
          header={product.asset}
          title={product.name}
          subtitle={product.category}
          link={`/product/${product.ISIN}`}
        />
      </LazyLoad>
    </Grid>
  ));
}

const Dashboard: NextPage<DashboardProps> = function Dashboard({ products }) {
  const [state, setState] = React.useState<DashboardState>({
    pattern: '',
  });

  const onInputChange = React.useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setState(
      (prevState) => ({
        ...prevState,
        pattern: event.target.value,
      }),
    );
  }, [setState]);

  const filteredProducts = Products.getFilteredProducts(products, state.pattern);
  const productCards = getProductCards(filteredProducts);

  return (
    <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={8} lg={9}>
          <Paper className="title-paper">
            <Title>Search</Title>
            <TextField label="Product" variant="standard" onChange={onInputChange} />
          </Paper>
        </Grid>
        <Grid item xs={12} md={4} lg={3}>
          <Paper className="stats-paper">
            <Stats />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className="cards-paper">
            <Grid container spacing={2}>
              {productCards}
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Dashboard;
