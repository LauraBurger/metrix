import React from 'react';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Chip from '@mui/material/Chip';
import LazyLoad from 'react-lazyload';
import { useRouter } from 'next/router';
import EqualizerIcon from '@mui/icons-material/Equalizer';
import QrCodeIcon from '@mui/icons-material/QrCode';
import MonetizationOnRoundedIcon from '@mui/icons-material/MonetizationOnRounded';
import ErrorPage from 'next/error';
import InfoIcon from '@mui/icons-material/Info';
import HeightIcon from '@mui/icons-material/Height';
import Title from '../../components/title';
import ProgressBar from '../../components/progressBar';
import Card from '../../components/card';
import data from '../../assets/products.json';
import { Product as ProductType } from '../../types/index';
import { Timeframe, Timeframes } from '../../types/constants';
import Products from '../../utils/products';
import Colors from '../../utils/colors';

interface ProductProps {
  products: ProductType[],
}

export async function getStaticProps() {
  return {
    props: {
      products: data,
    },
  };
}

export async function getStaticPaths() {
  const paths = data.map((product) => ({ params: { isin: product.ISIN } }));

  return {
    paths,
    fallback: false,
  };
}

function getProgressBars(product: ProductType) {
  const productTimeframes: Timeframe[] = Timeframes.filter((timeframe) => product[timeframe]);
  return productTimeframes.map((timeframe: Timeframe) => (
    <ProgressBar
      key={timeframe}
      title={timeframe}
      value={product[timeframe]!}
    />
  ));
}

function getProductCards(products: ProductType[]) {
  return products.map((product) => (
    <Grid item xs={12} sm={12} md={4} lg={4} key={product.ISIN}>
      <LazyLoad scrollContainer=".cards-container">
        <Card
          classNames={{ card: 'card', title: 'truncate' }}
          color={Colors.getColor(product.asset)}
          header={product.asset}
          title={product.name}
          subtitle={product.category}
          link={`/product/${product.ISIN}`}
        />
      </LazyLoad>
    </Grid>
  ));
}

function Product({ products }: ProductProps) {
  const router = useRouter();
  const isin = router.query.isin as string;

  if (isin) {
    const product = Products.getProduct(data, isin);
    if (product) {
      const similarProducts = Products.getSimilarProducts(
        products,
        product.asset,
        product.category,
      );
      const productCards = getProductCards(similarProducts);
      return (
        <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
          <Grid container spacing={3}>
            <Grid item xs={12} md={8} lg={8}>
              <Paper className="title-paper">
                <Stack direction="row" spacing={1}>
                  <MonetizationOnRoundedIcon className="icon" />
                  <Title>
                    {product.name}
                  </Title>
                </Stack>
                <Grid
                  container
                  rowSpacing={1}
                  columnSpacing={{ xs: 1, sm: 2, md: 3 }}
                  sx={{ mt: 1, mb: 1 }}
                >
                  <Grid item xs={12} sx={{ pt: 0 }}>
                    <Stack direction="row" spacing={1}>
                      <Chip label={product.asset} color="primary" />
                      <Chip label={product.category} color="primary" />
                    </Stack>
                    <Stack direction="row" spacing={1} sx={{ mt: 4 }}>
                      <Chip icon={<QrCodeIcon />} label={product.ISIN} color="primary" variant="outlined" />
                      <Chip icon={<InfoIcon />} label={product.WKN} color="primary" variant="outlined" />
                      <Chip icon={<HeightIcon />} label={`${product.fundSize}M`} color="primary" variant="outlined" />
                    </Stack>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
            <Grid item xs={12} md={4} lg={4}>
              <Paper className="stats-paper">
                <Stack direction="row" spacing={1} sx={{ pb: 2 }}>
                  <EqualizerIcon className="icon" />
                  <Title>Performances</Title>
                </Stack>
                {getProgressBars(product)}
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Paper className="cards-paper">
                <Grid container spacing={2}>
                  {productCards}
                </Grid>
              </Paper>
            </Grid>
          </Grid>
        </Container>
      );
    }
  }

  return (<ErrorPage statusCode={404} />);
}

export default Product;
