/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Box from '@mui/material/Box';
import DashboardIcon from '@mui/icons-material/Dashboard';
import LeaderboardOutlinedIcon from '@mui/icons-material/LeaderboardOutlined';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from '@mui/material/Toolbar';
import ListItem from '../components/listItem';
import Copyright from '../components/copyright';
import Drawer from '../components/drawer';
import AppBar from '../components/appBar';
import { ListItemProps } from '../types';

const dataListItems = [
  {
    href: '/dashboard',
    primary: 'Dashboard',
    icon: <DashboardIcon />,
  },
  {
    href: '/leaders/1M',
    primary: 'Leaders',
    icon: <LeaderboardOutlinedIcon />,
  },
];

function getListItems(items: ListItemProps[]) {
  return items.map((item: ListItemProps) => (
    <ListItem
      key={item.primary}
      href={item.href}
      icon={item.icon}
      primary={item.primary}
    />
  ));
}

const ListItems = getListItems(dataListItems);

function MyApp({ Component, pageProps }: AppProps) {
  const [open, setOpen] = React.useState(false);
  const toggleDrawer = () => {
    setOpen(!open);
  };

  return (
    <ThemeProvider theme={createTheme()}>
      <Box sx={{ display: 'flex' }}>
        <CssBaseline />
        <AppBar open={open} toggleDrawer={toggleDrawer} />
        <Drawer open={open} toggleDrawer={toggleDrawer} mainListItems={ListItems} />
        <Box
          component="main"
          className="cards-container"
          sx={{
            backgroundColor: (theme) => (theme.palette.mode === 'light'
              ? theme.palette.grey[100]
              : theme.palette.grey[900]),
          }}
        >
          <Toolbar />
          <Component {...pageProps} />
          <Copyright />
        </Box>
      </Box>
    </ThemeProvider>
  );
}

export default MyApp;
