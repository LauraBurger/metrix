const prefix = process.env.NODE_ENV === 'production' ? '/metrix' : '';

module.exports = {
  reactStrictMode: true,
  basePath: prefix,
  assetPrefix: prefix,
}
