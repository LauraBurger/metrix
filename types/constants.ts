export const Timeframes = ['1M', '3M', '6M', '1Y', '3Y', '5Y'] as const;
export type Timeframe = typeof Timeframes[number];
