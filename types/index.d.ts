export interface Product {
  'name': string,
  'ISIN': string,
  'WKN': string | null,
  '1M': number | null, // FIXME: remove null?
  '3M': number | null,
  '6M': number | null,
  '1Y': number | null,
  '3Y': number | null,
  '5Y': number | null,
  'asset': string,
  'category': string | null,
  'fundSize': number | null, // FIXME: remove null ?
}

export interface ListItemProps {
  href: string,
  primary: string,
  icon: React.ReactNode,
}
