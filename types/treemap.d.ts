interface TreeMapItem {
  'name': string,
  'id': string,
}

interface TreeMapNodeItem extends TreeMapItem {
  'parent': string,
  'prop': number,
}

export type TreeMapAsset = TreeMapItem;
export type TreeMapProduct = TreeMapNodeItem;
export type TreeMapData = Array<TreeMapItem>;
