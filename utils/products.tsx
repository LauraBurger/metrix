import { Product } from '../types';
import { Timeframe } from '../types/constants';
import { TreeMapProduct, TreeMapAsset } from '../types/treemap';

export default {
  getFilteredProducts(products: Product[], pattern: string): Product[] {
    return pattern
      ? products.filter((product) => product.name.toLowerCase().includes(pattern.toLowerCase().trim()))
      : products;
  },
  getSimilarProducts(products: Product[], asset: string, category: string | null): Product[] {
    let similarProducts: Product[] = products.filter((product) => product.asset === asset);
    if (category) {
      similarProducts = similarProducts.filter((product) => product.category === category);
    }
    return similarProducts;
  },
  getTreeMapProducts(data: Product[], timeframe: Timeframe) {
    const products: Product[] = data.filter((product) => product[timeframe]);
    products.sort((a, b) => a[timeframe]! - b[timeframe]!);
    const size = products.length;
    const trirdQuartile = Math.round((3 * size) / 4);
    const topProducts = products.slice(trirdQuartile);
    const treeMapData: TreeMapProduct[] = topProducts.map((product) => ({
      id: product.ISIN,
      name: product.name,
      parent: product.asset,
      prop: product[timeframe]!,
    }));
    return treeMapData;
  },
  getTreeMapAssets(data: Product[], timeframe: Timeframe) {
    const products: Product[] = data.filter((product) => product[timeframe]);
    const assets: string[] = products.map((product) => product.asset);
    const uniqueAssets: string[] = [...new Set(assets)];
    const treeMapData: TreeMapAsset[] = uniqueAssets
      .map((asset) => ({
        id: asset,
        name: asset,
      }));
    return treeMapData;
  },
  getProduct(data: Product[], isin: string) {
    const product = data.find((element) => element.ISIN === isin) || null;
    return product;
  },
};
