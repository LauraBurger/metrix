interface IColors {
  [key: string]: string
}

const colors: IColors = {};

function getPastelColor() {
  const r = (Math.round(Math.random() * 127) + 127).toString(16);
  const g = (Math.round(Math.random() * 127) + 127).toString(16);
  const b = (Math.round(Math.random() * 127) + 127).toString(16);
  return `#${r}${g}${b}`;
}

export default {
  getColor(identifier: string) {
    if (!colors[identifier]) {
      colors[identifier] = getPastelColor();
    }
    return colors[identifier];
  },
};
